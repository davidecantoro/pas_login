package it.unisalento.pas.smartcitywastemanagement.restcontrollers;

import it.unisalento.pas.smartcitywastemanagement.domain.User;
import it.unisalento.pas.smartcitywastemanagement.dto.AuthenticationResponseDTO;
import it.unisalento.pas.smartcitywastemanagement.dto.LoginDTO;
import it.unisalento.pas.smartcitywastemanagement.dto.UserDTO;
import it.unisalento.pas.smartcitywastemanagement.exceptions.UserNotFoundException;
import it.unisalento.pas.smartcitywastemanagement.repositories.UserRepository;
import it.unisalento.pas.smartcitywastemanagement.security.JwtUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;

import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static it.unisalento.pas.smartcitywastemanagement.configuration.SecurityConfig.passwordEncoder;

@CrossOrigin
@RestController

@RequestMapping("/api/users")
public class UserRestController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager; // lo prende dal bean in SecurityConfig

    @Autowired
    private JwtUtilities jwtUtilities;


    @RequestMapping(value="/", method= RequestMethod.GET)
    public List<UserDTO> getAll() {

        List<UserDTO> utenti = new ArrayList<>();

        for(User user : userRepository.findAll()) {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setNome(user.getNome());
            userDTO.setCognome(user.getCognome());
            userDTO.setEmail(user.getEmail());
            userDTO.setBirthdate(user.getBirthdate());
            userDTO.setUsername(user.getUsername());
            userDTO.setRole(user.getRole());
            utenti.add(userDTO);
        }

        return utenti;
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN','EMPLOYEE')")
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public UserDTO get(@PathVariable String id)  throws UserNotFoundException {

        System.out.println("ARRIVATO L'ID: "+id);

        Optional<User> optUser = userRepository.findById(id);

        if(optUser.isPresent()) {
            User user = optUser.get();

            UserDTO user1 = new UserDTO();
            user1.setNome(user.getNome());
            user1.setCognome(user.getCognome());
            user1.setEmail(user.getEmail());
            user1.setBirthdate(user.getBirthdate());
            user1.setId(user.getId());
            user1.setRole(user.getRole());

            return user1;
        }
        //throw new UserNotFoundException();
        else {

            UserDTO user1 = new UserDTO();
            user1.setNome("ciao");
            user1.setCognome("ciao");
            user1.setEmail("ciao");

            user1.setId("ciao");

            return user1;
        }
    }
    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @RequestMapping(value="/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO post(@RequestBody UserDTO userDTO) {

        User newUser = new User();
        newUser.setNome(userDTO.getNome()); // DTO è usato per trasferire dati tipo dalla richeista post con posteman viene mandato al DTO
        newUser.setCognome(userDTO.getCognome());
        newUser.setEmail(userDTO.getEmail());
        newUser.setBirthdate(userDTO.getBirthdate());
        newUser.setRole(userDTO.getRole());
        newUser.setUsername(userDTO.getUsername());
        newUser.setPassword(passwordEncoder().encode(userDTO.getPassword()));

        newUser = userRepository.save(newUser);
        System.out.println("L'ID DEL NUOVO UTENTE E': "+newUser.getId());

        userDTO.setId(newUser.getId());
        userDTO.setPassword(null);
        return userDTO;
    }
    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @RequestMapping(value="/find", method = RequestMethod.GET)
    public List<UserDTO> findByCognome(@RequestParam String cognome) {

        List<User> result = userRepository.findByCognome(cognome);
        List<UserDTO> utenti = new ArrayList<>();

        for(User user: result) {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setNome(user.getNome());
            userDTO.setCognome(user.getCognome());
            userDTO.setEmail(user.getEmail());
            userDTO.setBirthdate(user.getBirthdate());
            userDTO.setRole(user.getRole());
            utenti.add(userDTO);
        }

        return utenti;
    }

    @RequestMapping(value="/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginDTO loginDTO) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginDTO.getUsername(),
                            loginDTO.getPassword()
                    )
            );

            User user = userRepository.findByUsername(authentication.getName());
            SecurityContextHolder.getContext().setAuthentication(authentication);

            String role = user.getRole();
            String id = user.getId();
            final String jwt = jwtUtilities.generateToken(user.getUsername(), role, id);

            return ResponseEntity.ok(new AuthenticationResponseDTO(jwt));

        } catch (UsernameNotFoundException | BadCredentialsException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Login fallito");
        }
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN','COMUNE')")
    @GetMapping("/by-role")
    public Page<UserDTO> getUsersByRole(@RequestParam String role,
                                        @RequestParam(defaultValue = "0") int page,
                                        @RequestParam(defaultValue = "3") int size) {
        org.springframework.data.domain.Pageable paging = PageRequest.of(page, size);
        Page<User> pagedResult = userRepository.findByRole(role,paging);

        Page<UserDTO> pagedDto = pagedResult.map(user -> {
            UserDTO dto = new UserDTO();
            dto.setId(user.getId());
            dto.setNome(user.getNome());
            dto.setCognome(user.getCognome());
            dto.setBirthdate(user.getBirthdate());
            dto.setEmail(user.getEmail());

            return dto;
        });

        return pagedDto;
    }
    @PostMapping("/register-citizen")
    public ResponseEntity<?> registerCitizen(@RequestBody UserDTO userDTO) {
        // Controlla se l'username esiste già nel database
        User existingUser = userRepository.findByUsername(userDTO.getUsername());
        if (existingUser != null) {
            // Se l'username esiste già, restituisci un messaggio di errore con codice 409
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("username già in uso");
        }

        try {
            // Imposta il ruolo a "CITIZEN"
            userDTO.setRole("citizen");

            // Converti UserDTO in entità User
            User newUser = new User();
            newUser.setNome(userDTO.getNome());
            newUser.setCognome(userDTO.getCognome());
            newUser.setUsername(userDTO.getUsername());
            newUser.setEmail(userDTO.getEmail());
            newUser.setPassword(passwordEncoder().encode(userDTO.getPassword()));
            newUser.setBirthdate(userDTO.getBirthdate());

            newUser.setRole(userDTO.getRole());

            // Salva il nuovo utente nel database
            newUser = userRepository.save(newUser);

            // Crea una risposta senza includere la password
            userDTO.setId(newUser.getId());
            userDTO.setPassword(null); // Non restituire la password

            return ResponseEntity.status(HttpStatus.CREATED).body(userDTO);
        } catch (Exception e) {
            // Gestisci l'eccezione
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Errore nella registrazione dell'utente");
        }
    }

}


// form ---> userDto ----> user ---> mongodb (attraverso userRepository )
//mongo db ---> user ( attraverso userRepository) ---> userDTO
// userRepository accede al db con i metodi preimpostati, e restituisce un User perche nell'interfaccia abbiamo messo <User, String>