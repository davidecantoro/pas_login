package it.unisalento.pas.smartcitywastemanagement.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import it.unisalento.pas.smartcitywastemanagement.domain.User;


import java.util.List;

public interface UserRepository extends MongoRepository<User, String> { // string è il tipo della chiave primaria cioe @Id

    public List<User> findByCognome(String cognome);

    public User findByUsername(String username);
    Page<User> findByRole(String role, Pageable pageable);

}
